<?php

namespace Megacoders\AdminBundle\Source;

use Doctrine\ORM\Query;
use Exporter\Source\DoctrineORMQuerySourceIterator;
use Symfony\Component\PropertyAccess\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\TranslatorInterface;

class TranslatableQuerySourceIterator extends DoctrineORMQuerySourceIterator
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var array
     */
    protected $translatableFields = [];

    /**
     * @var string
     */
    protected $arrayDelimiter;

    /**
     * TranslatableQuerySourceIterator constructor.
     * @param TranslatorInterface $translator
     * @param Query $query
     * @param array $fields
     * @param string $dateTimeFormat
     * @param string $arrayDelimiter
     */
    public function __construct(TranslatorInterface $translator, Query $query, array $fields, $dateTimeFormat = 'r', $arrayDelimiter = '; ')
    {
        $this->translator = $translator;
        $this->arrayDelimiter = $arrayDelimiter;

        $parsedFields = [];

        foreach ($fields as $name => $field) {
            $name = $this->translator->trans($name);

            if (is_string($field) && preg_match('#^trans\((.*)\)$#', $field, $match)) {
                $field = $match[1];
                $this->translatableFields[] = $name;
            }

            $parsedFields[$name] = $field;
        }

        parent::__construct($query, $parsedFields, $dateTimeFormat);
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        $current = $this->iterator->current();

        $data = array();

        foreach ($this->propertyPaths as $name => $propertyPath) {
            try {
                $data[$name] = $this->getTranslatedValue(
                    $name,
                    $this->propertyAccessor->getValue($current[0], $propertyPath)
                );

            } catch (UnexpectedTypeException $e) {
                //non existent object in path will be ignored
                $data[$name] = null;
            }
        }

        $this->query->getEntityManager()->getUnitOfWork()->detach($current[0]);

        return $data;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return null|string
     */
    protected function getTranslatedValue($name, $value)
    {
        if (is_array($value) || $value instanceof \Traversable) {
            $array = [];

            foreach ($value as $item) {
                $array[] = $this->getTranslatedValue($name, $item);
            }

            return implode($this->arrayDelimiter, $array);
        }

        if (is_bool($value)) {
            return in_array($name, $this->translatableFields)
                ? $this->translator->trans($value ? 'Yes' : 'No')
                : $this->getValue($value);
        }

        $result = $this->getValue($value);

        return in_array($name, $this->translatableFields) ? $this->translator->trans($result) : $result;
    }
}
