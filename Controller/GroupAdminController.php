<?php

namespace Megacoders\AdminBundle\Controller;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr;
use Megacoders\AdminBundle\Admin\GroupAdmin;
use Megacoders\AdminBundle\Entity\Group;
use Megacoders\AdminBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class GroupAdminController extends BaseAdminController
{
    /** @var GroupAdmin */
    protected $admin;

    /**
     * @param Request $request
     * @param Group $object
     * @return null
     */
    protected function preDelete(Request $request, $object)
    {
        /** @var Session $session */
        $session = $this->get('session');

        /** @var EntityManager $entityManager */
        $entityManager = $this->get('doctrine.orm.entity_manager');

        $expr = new Expr();
        $usersCount = $entityManager->getRepository(User::class)->createQueryBuilder('u')
            ->select('count(u.id)')
            ->leftJoin('u.groups', 'g')
            ->where($expr->eq('g.id', $object->getId()))
            ->getQuery()
                ->getSingleScalarResult()
        ;

        if ($usersCount) {
            $session->getFlashBag()->add(
                'sonata_flash_error',
                sprintf('You can not remove this group because it includes %d member(s)', $usersCount)
            );

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        return null;
    }
}
