<?php

namespace Megacoders\AdminBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BaseAdminController extends CRUDController
{
    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function cloneAction($id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('Unable to find the object with id : %s', $id));
        }

        $clonedObject = clone $object;
        $this->admin->create($clonedObject);

        $this->addFlash(
            'sonata_flash_success',
            sprintf('Item "%s" cloned successfully to "%s".', (string) $object, (string) $clonedObject)
        );

        return new RedirectResponse($this->getCloneRedirectUrl($clonedObject));
    }

    /**
     * @param object $object
     * @return string
     */
    protected function getCloneRedirectUrl($object)
    {
        return $this->admin->generateUrl('list');
    }

    /**
     * @param string $class
     * @return EntityRepository
     */
    protected function getEntityRepository($class)
    {
        return $this->get('doctrine.orm.entity_manager')->getRepository($class);
    }
}
