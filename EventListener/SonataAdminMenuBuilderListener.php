<?php

namespace Megacoders\AdminBundle\EventListener;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Event\ConfigureMenuEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class SonataAdminMenuBuilderListener
{
    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * @var CamelCaseToSnakeCaseNameConverter
     */
    private $stringConverter;

    /**
     * SonataAdminMenuBuilderListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->stringConverter = new CamelCaseToSnakeCaseNameConverter();
    }

    /**
     * @param ConfigureMenuEvent $event
     */
    public function configureMenu(ConfigureMenuEvent $event)
    {
        $bundleNames = array_keys($this->container->getParameter('kernel.bundles'));

        foreach ($bundleNames as $bundleName) {
            $bundleSnakeCasedName = current(
                explode('_bundle', $this->stringConverter->normalize(lcfirst($bundleName)))
            );

            $parameterName = implode('.', [$bundleSnakeCasedName, 'admin_menu_routes_fix_map']);

            if ($this->container->hasParameter($parameterName)) {
                $menuFixMap = $this->container->getParameter($parameterName);

                foreach ($menuFixMap as $name => $routes) {
                    $item = $this->findMenuItem($event->getMenu(), $name);

                    if ($item) {
                        $item->setExtra('routes', array_merge(
                            $item->getExtra('route') ? [$item->getExtra('route')] : ($item->getExtra('routes') ?: []),
                            $routes
                        ));
                    }
                }
            }
        }
    }

    /**
     * @param MenuItemInterface $menu
     * @param string $name
     * @return MenuItemInterface|null
     */
    private function findMenuItem(MenuItemInterface $menu, $name)
    {
        foreach ($menu->getChildren() as $child) {
            if ($child->getName() == $name) {
                return $child;
            }

            if ($child->hasChildren() && $item = $this->findMenuItem($child, $name)) {
                return $item;
            }
        }

        return null;
    }
}
