<?php

namespace Megacoders\AdminBundle\Entity;


interface EntityInterface
{
    /**
     * @return mixed
     */
    public function getId();
}
