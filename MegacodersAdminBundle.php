<?php

namespace Megacoders\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MegacodersAdminBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
