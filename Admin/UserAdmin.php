<?php

namespace Megacoders\AdminBundle\Admin;

use FOS\UserBundle\Model\UserManagerInterface;
use Megacoders\AdminBundle\Entity\User;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;

class UserAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'user';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'username',
    );

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @param User $user
     */
    public function preUpdate($user)
    {
        $this->userManager->updateCanonicalFields($user);
        $this->userManager->updatePassword($user);
    }

    /**
     * @param UserManagerInterface $userManager
     */
    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username', null, ['label' => 'admin.entities.user.username'])
            ->add('email', null, ['label' => 'admin.entities.user.email'])
            ->add('phone', null, ['label' => 'admin.entities.user.phone'])
            ->add('groups', null, ['label' => 'admin.entities.user.groups'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('username', null, ['label' => 'admin.entities.user.username'])
            ->add('name', null, ['label' => 'admin.entities.user.name'])
            ->add('groups', null, ['label' => 'admin.entities.user.groups'])
            ->add('enabled', null, ['label' => 'admin.entities.user.enabled'])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => [], 'delete' => []]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $user = $this->getUser();
        $isSuperAdmin = $user->isSuperAdmin();

        $formMapper
            ->with('admin.labels.user')
                ->add('username', null, ['label' => 'admin.entities.user.username'])
                ->add('lastName', null, ['label' => 'admin.entities.user.last_name'])
                ->add('firstName', null, ['label' => 'admin.entities.user.first_name'])
                ->add('middleName', null, ['label' => 'admin.entities.user.middle_name'])
                ->add('phone', null, ['label' => 'admin.entities.user.phone'])
                ->add('email', null, ['label' => 'admin.entities.user.email'])
                ->add('created', 'sonata_type_date_picker', [
                    'label' => 'admin.entities.user.created',
                    'disabled' => !$isSuperAdmin,
                    'format' => 'dd.MM.y HH:mm'
                ])
                ->add('enabled', null, ['label' => 'admin.entities.user.enabled'])
                ->add('groups', null, ['label' => 'admin.entities.user.groups'])
                ->add('plainPassword', 'repeated', [
                    'required' => false,
                    'type' => 'password',
                    'options' => ['translation_domain' => 'FOSUserBundle'],
                    'first_options' => ['label' => 'form.new_password'],
                    'second_options' => ['label' => 'form.new_password_confirmation'],
                    'invalid_message' => 'fos_user.password.mismatch',
                ])
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        /** @var User $object */

        /** Check for user with same username. */
        $userByUsername = $this->userManager->findUserByUsername($object->getUsername());

        if ($userByUsername !== null && $userByUsername->getId() != $object->getId()) {
            $errorElement
                ->with('username')
                    ->addViolation('This value is already used.')
                ->end()
            ;
        }

        /** Check for user with same email. */
        $userByEmail = $this->userManager->findUserByEmail($object->getEmail());

        if ($userByEmail !== null && $userByEmail->getId() != $object->getId()) {
            $errorElement
                ->with('email')
                    ->addViolation('This value is already used.')
                ->end()
            ;
        }

        /** Check for new user password. */
        if (!$object->getId()) {
            $errorElement
                ->with('plainPassword')
                    ->assertNotBlank()
                ->end()
            ;
        }
    }
}
