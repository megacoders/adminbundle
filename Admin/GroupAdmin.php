<?php

namespace Megacoders\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class GroupAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'group';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    /**
     * @param bool $subRoles
     * @return array
     */
    public function getRolesChoices($subRoles = false)
    {
        $rolesHierarchy = $this->getRolesHierarchy();
        $flatRoles = [];

        foreach ($rolesHierarchy as $name => $roles) {
            if (empty($roles)) {
                continue;
            }

            if (!isset($flatRoles[$name])) {
                $flatRoles[$name] = $subRoles ? [] : $name;
            }

            if ($subRoles) {
                foreach ($roles as $role) {
                    if (!isset($flatRoles[$name][$role])) {
                        $flatRoles[$name][$role] = $role;
                    }
                }
            }
        }

        return $flatRoles;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, ['label' => 'admin.entities.group.name']);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'admin.entities.group.name'])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => [], 'delete' => []]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('admin.labels.group')
                ->add('name', null, ['label' => 'admin.entities.group.name'])
                ->add('roles', 'choice', [
                    'label' => 'admin.entities.group.roles',
                    'choices'  => $this->getRolesChoices(),
                    'multiple' => true
                ])
            ->end()
        ;
    }
}
