<?php

namespace Megacoders\AdminBundle\Admin;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Megacoders\AdminBundle\Entity\User;
use Megacoders\AdminBundle\Source\TranslatableQuerySourceIterator;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridInterface;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Route\RouteCollection;

class BaseAdmin extends AbstractAdmin
{
    /**
     * @var string
     */
    protected $dateFormat = 'y-m-d H:i';

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('clone', $this->getRouterIdParameter() .'/clone');
    }

    /**
     * @return User
     */
    protected function getUser()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @return array
     */
    protected function getRolesHierarchy()
    {
        $container = $this->getConfigurationPool()->getContainer();

        return $container->getParameter('security.role_hierarchy.roles');
    }

    /**
     * @return bool
     */
    public function isEditMode()
    {
        $subject = $this->getSubject();

        return $subject && $this->id($subject);
    }

    /**
     * @return bool
     */
    public function isCreateMode()
    {
        $subject = $this->getSubject();

        return $subject && !$this->id($subject);
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');

        return $entityManager;
    }

    /**
     * @param $class
     * @return EntityRepository
     */
    protected function getEntityRepository($class)
    {
        return $this->getEntityManager()->getRepository($class);
    }

    /**
     * @param string $name
     * @param array|string $context
     * @return bool
     */
    protected function isGrantedOnly($name, $context)
    {
        if ($this->getUser()->isSuperAdmin()) {
            return false;
        }

        $contextPart = strtoupper(is_array($context) ? current($context) : $context);
        $constrainPart = implode('_', ['ONLY', strtoupper($name)]);

        return
            (parent::isGranted($contextPart) && parent::isGranted($constrainPart)) ||
            parent::isGranted(implode('_', [$contextPart, $constrainPart]))
        ;
    }

    /**
     * @param DatagridInterface $datagrid
     * @param array $fields
     * @param null $firstResult
     * @param null $maxResult
     * @return TranslatableQuerySourceIterator
     */
    protected function getDoctrineORMQuerySourceIterator(DatagridInterface $datagrid, array $fields, $firstResult = null, $maxResult = null)
    {
        $datagrid->buildPager();
        $query = $datagrid->getQuery();

        $query->select('DISTINCT '.current($query->getRootAliases()));
        $query->setFirstResult($firstResult);
        $query->setMaxResults($maxResult);

        if ($query instanceof ProxyQueryInterface) {
            $query->addOrderBy($query->getSortBy(), $query->getSortOrder());

            $query = $query->getQuery();
        }

        return new TranslatableQuerySourceIterator(
            $this->getConfigurationPool()->getContainer()->get('translator'),
            $query, $fields, $this->dateFormat
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return [];//['csv'];
    }

    /**
     * {@inheritdoc}
     */
    public function getDataSourceIterator()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        return $this->getDoctrineORMQuerySourceIterator($datagrid, $this->getExportFields());
    }
}
