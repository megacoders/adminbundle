<?php

namespace Megacoders\AdminBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Sonata\AdminBundle\Model\ModelManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;

class IdArrayToCollectionTransformer implements DataTransformerInterface
{
    /**
     * @var ModelManagerInterface
     */
    protected $modelManager;

    /**
     * @var string
     */
    protected $className;

    /**
     * @param ModelManagerInterface $modelManager
     * @param string                $className
     */
    public function __construct(ModelManagerInterface $modelManager, $className)
    {
        $this->modelManager = $modelManager;
        $this->className = $className;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($collection)
    {
        $ids = [];

        if ($collection == null) {
            return $ids;
        }

        foreach ($collection as $object) {
            $ids[] = $this->modelManager->getNormalizedIdentifier($object);
        }

        return $ids;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($ids)
    {
        $objects = [];

        if (!is_array($ids) || $ids === null) {
            return $objects;
        }

        foreach ($ids as $id) {
            $objects[] = $this->modelManager->find($this->className, $id);
        }

        return $objects;
    }
}
