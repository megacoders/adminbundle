<?php

namespace Megacoders\AdminBundle\Form\DataTransformer;

use Sonata\AdminBundle\Model\ModelManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;

class IdToModelTransformer implements DataTransformerInterface
{
    /**
     * @var ModelManagerInterface
     */
    protected $modelManager;

    /**
     * @var string
     */
    protected $className;

    /**
     * @param ModelManagerInterface $modelManager
     * @param string                $className
     */
    public function __construct(ModelManagerInterface $modelManager, $className)
    {
        $this->modelManager = $modelManager;
        $this->className = $className;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($id)
    {
        if (empty($id) && !in_array($id, array('0', 0), true)) {
            return null;
        }

        return $this->modelManager->find($this->className, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($entity)
    {
        if (empty($entity)) {
            return null;
        }

        return $this->modelManager->getNormalizedIdentifier($entity);
    }
}
