<?php

namespace Megacoders\AdminBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class StringToUploadedFileTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        return ['fileName' => $value, 'file' => null];
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        return $value['fileName'];
    }
}
