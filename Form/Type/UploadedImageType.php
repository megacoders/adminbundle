<?php

namespace Megacoders\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class UploadedImageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return UploadedFileType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'uploaded_image';
    }
}
