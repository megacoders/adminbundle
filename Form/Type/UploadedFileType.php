<?php

namespace Megacoders\AdminBundle\Form\Type;

use Megacoders\AdminBundle\Form\DataTransformer\StringToUploadedFileTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UploadedFileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fileName', HiddenType::class)
            ->add('file', FileType::class, ['label' => false, 'required' => $options['required']])
            ->addModelTransformer(new StringToUploadedFileTransformer())
            ->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) use ($options) {
                $data = $event->getData();
                $file = $data['file'];

                if ($file instanceof UploadedFile) {
                    if ($options['delete_previous']) {
                        $oldFileInfo = pathinfo($data['fileName']);
                        $oldFilePath = $options['upload_directory'] .'/' .$oldFileInfo['basename'];

                        if (is_file($oldFilePath)) {
                            unlink($oldFilePath);
                        }
                    }

                    $fileName = md5(uniqid()) .'.' .$file->guessExtension();
                    $file->move($options['upload_directory'], $fileName);

                    $data['file'] = null;
                    $data['fileName'] = $options['public_directory'] .'/' .$fileName;
                }

                $event->setData($data);
            })
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $fileName = $form->get('fileName')->getData();

        $view->vars['public_url'] = $fileName ?: null;

        if (isset($view->children['file'])) {
            $view->children['file']->vars = array_replace($view->vars, $view->children['file']->vars);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'compound' => true,
            'delete_previous' => true,
            'error_bubbling' => false
        ]);

        $resolver->setRequired([
            'upload_directory',
            'public_directory'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'uploaded_file';
    }
}
