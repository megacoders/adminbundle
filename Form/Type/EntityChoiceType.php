<?php

namespace Megacoders\AdminBundle\Form\Type;

use Megacoders\AdminBundle\Entity\ListEntityInterface;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

class EntityChoiceType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * EntityChoiceType constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /** @var ClassMetadata[] $metas */
        $metas = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $entities = [];

        foreach ($metas as $meta) {
            if ($meta->getReflectionClass()->implementsInterface(ListEntityInterface::class)) {
                $entities[$meta->getName()] = $this->translator->trans($meta->getName());
            }
        }

        $resolver->setDefault('choices', array_flip($entities));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
