<?php

namespace Megacoders\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Validator\Constraints\File;
use \XMLReader;


class UploadedSvgType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) use ($options) {
                $data = $event->getData();

                if (!empty($data['fileName'])) {
                    $pathInfo = pathinfo($data['fileName']);
                    $oldFileName = $pathInfo['basename'];
                    $path = $options['upload_directory'] .'/' . $oldFileName;

                    if ($this->isSvgFile($path)) {
                        $newFileName = $pathInfo['filename'] . '.svg';
                        if (rename($path, $options['upload_directory'] .'/' . $newFileName)) {
                            $data['fileName'] = $options['public_directory'] .'/' . $newFileName;
                        }
                    }
                    else {
                        $data['fileName'] = null;
                        $form = $event->getForm();
                        $form->addError(new FormError('File is not svg'));
                        if (is_file($path)) {
                            unlink($path);
                        }
                    }
                }

                $event->setData($data);
            })
        ;
    }

    /**
     * @param $path string
     * @return bool
     */
    protected function isSvgFile( $path )
    {
        $xml = XMLReader::open( $path );
        $xml->setParserProperty(XMLReader::VALIDATE, true);

        if (!$xml->isValid()) {
            return false;
        }

        $content = file_get_contents($path);
        if (!preg_match('/^<svg/', $content)) {
            return false;
        }

        return true;

    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return UploadedFileType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'uploaded_svg';
    }
}
