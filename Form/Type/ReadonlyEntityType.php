<?php

namespace Megacoders\AdminBundle\Form\Type;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReadonlyEntityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'disabled' => true,
            'required' => false,
            'admin' => null,
            'admin_action' => 'edit',
            'as_link' => false
        ]);

        $resolver->setAllowedTypes('admin', ['null', AbstractAdmin::class]);
        $resolver->setAllowedValues('admin_action', ['show', 'edit']);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['admin'] = $options['admin'];
        $view->vars['admin_action'] = $options['admin_action'];
        $view->vars['as_link'] = $options['as_link'];
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return EntityType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'readonly_entity';
    }
}
